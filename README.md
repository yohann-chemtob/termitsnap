# Termite IR Camera

Research project to take picture inside a termite nest

## Installation

From a Lite Raspbian

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential python3-dev python3-pip python3-picamera python3-rpi.gpio 
sudo apt-get install git
sudo pip3 install Adafruit_Python_DHT
```

To make it start every time the Pi boot, edit the /etc/rc.local with 
```
sudo nano /etc/rc.local
```
and add : 
```
sudo python /home/pi/termitsnap/cameraledandtemp.py &
sudo python /home/pi/termitsnap/listen-for-shutdown.py &
```
