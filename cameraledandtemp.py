#!/usr/bin/env python3

__author__ = "Yohann Chemtob"
__license__ = "WTFPL"
__version__ = "0.1"

from picamera import PiCamera
import time
import RPi.GPIO as GPIO
import Adafruit_DHT

camera = PiCamera()			#initialize camera 
sensor = Adafruit_DHT.DHT22		#initialize temperature probe

pin = 18				#pin (BCM_code) of temperature probe
#__________https://fr.pinout.xyz/

GPIO.setmode(GPIO.BCM)			#choose BCM_mode
GPIO.setup(17, GPIO.OUT)		#set pin 17 (BCM_mode) for output corrent (this is connected to the relai)
camera.resolution = (3280, 2464)	
camera.iso = 50				#bug, it seems white balance change in time!!!
DELTA=60.0 				#time between pictures (recording)

#print('Press Ctrl-C to quit.')		#of no use
starttime=time.time()
namefile=time.strftime("/home/pi/timelapse01/tmphydro-%Y-%m-%dT%H%M%S.txt", time.gmtime(starttime))  #create a new file for record entitled with exact date up to sec
namefile_debug=time.strftime("/home/pi/timelapse01/th_debug-%Y-%m-%dT%H%M%S.txt", time.gmtime(starttime))  #create a twin file to check temperature after led has been on
f=open(namefile,"w+")
f.write("Time     temperature        humidity"+'\n')
f.close()

f_debug=open(namefile_debug,"w+")
f_debug.write("Time     temperature        humidity"+'\n')
f_debug.close()


while True:
    tmptime = time.gmtime()		#get present time
#______________record temperature__________________________
    f=open(namefile,"a")						#re-open file
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)	#probe library
    if humidity is not None and temperature is not None:
        print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)) #this is for debug, only print if you are sshing to the RaPi
        f.write(time.strftime("%Y-%m-%dT%H%M%S", tmptime)+"	{0:0.1f}	{1:0.1f}".format(temperature, humidity)+'\n')	
    else:
        print('Failed to get reading. Try again!')	#this is for debug, only print if you are sshing to the RaPi
        f.write(time.strftime("%Y-%m-%dT%H%M%S", tmptime)+"     NaN        NaN"+'\n')
    f.close()
#______________take picture__________________________
    time.sleep(10) 							# to avoid disturbence from the LED
    GPIO.output(17, GPIO.HIGH)		#send current to the relai, thus to relai, thus turn on the IR led
    time.sleep(0.5)			#wait for led to get stable enough
    tmpnamefile = time.strftime("%Y-%m-%dT%H%M%S.jpg", tmptime)		#create img_name
    camera.capture('/home/pi/timelapse01/'+tmpnamefile)			#take a picture and store in argument 
    time.sleep(0.5)							#wait for the ccd to complete taking picture (just to be safe)
    GPIO.output(17, GPIO.LOW)						#turn off led
#______________re-check temperature for debug__________________________
    f_debug=open(namefile,"a")						#re-open file
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)	#probe library
    if humidity is not None and temperature is not None:
        print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)) #this is for debug, only print if you are sshing to the RaPi
        f_debug.write(time.strftime("%Y-%m-%dT%H%M%S", tmptime)+"	{0:0.1f}	{1:0.1f}".format(temperature, humidity)+'\n')	
    else:
        print('Failed to get reading. Try again!')	#this is for debug, only print if you are sshing to the RaPi
        f_debug.write(time.strftime("%Y-%m-%dT%H%M%S", tmptime)+"     NaN        NaN"+'\n')
    f_debug.close()

    time.sleep(DELTA - ((time.time() - starttime) % DELTA))